package pal1;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Stack;
import java.util.StringTokenizer;

public class Main1 {

    static LinkedHashMap<String, Node> nodes = new LinkedHashMap<String, Node>();
    static ArrayList<String> nameOfNodes = new ArrayList<String>();

    public static void main(String args[]) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("input.txt"));
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Node n = null;
        String nameOfNode, subNodes, line;
        int index;
        while ((line = br.readLine()) != null) {
            if (line.charAt(0) == '\t') {
                n.commands.append("\n").append(line);
            } else {
                index = line.indexOf(":");
                nameOfNode = line.substring(0, index);
                subNodes = line.substring(index + 1, line.length());
                n = new Node(nameOfNode);
                n.defined = true;
                nodes.put(nameOfNode, n);
                nameOfNodes.add(nameOfNode);
                if (subNodes.length() > 0) {
                    StringTokenizer tokenizer = new StringTokenizer(subNodes);
                    while (tokenizer.hasMoreTokens()) {
                        n.subNodes.add(tokenizer.nextToken());
                    }
                }
            }
        }
        DFS();
        printOutput();
    }

    public static void DFS() {
        Stack<Node> stack = new Stack<Node>();
        Node rootNode = (Node) nodes.get(nameOfNodes.get(0));
        stack.push(rootNode);
        rootNode.open = true;
        boolean hasUnvisitedChild = false;
        while (!stack.empty()) {
            Node v = stack.peek();
            v.unvisited = false;
            hasUnvisitedChild = false;
            for (int i = 0; i < v.subNodes.size(); i++) {
                Node child = nodes.get(v.subNodes.get(i));
                if (!child.unvisited && child.open) {
                    System.out.println("ERROR");
                    System.exit(0);
                } else if (child.unvisited == true) {
                    child.open = true;
                    stack.push(child);
                    hasUnvisitedChild = true;
                }
            }
            if (!hasUnvisitedChild) {
                v.open = false;
                stack.pop();
            }
        }
//        rootNode.open = true;
//        rootNode.unvisited = false;
//        Node node, child;
//        while (!stack.isEmpty()) {
//            node = (Node) stack.peek();
//            if (node.defined) {
//                child = getUnvisitedChildNode(node);
//                if (child != null) {
//                    child.open = true;
//                    child.unvisited = false;
//                    stack.push(child);
//                } else {
//                    node.open = false;
//                    stack.pop();
//                }
//            } else {
//                System.out.println("ERROR");
//                System.exit(0);
//            }
//
//        }

    }

    public static void printOutput() throws IOException {
        StringBuilder sb = new StringBuilder();
//        OutputStream out = new BufferedOutputStream(System.out);
        Node n = null;
        for (int i = 0; i < nodes.size(); i++) {
            n = (Node) nodes.get(nameOfNodes.get(i));
            if (n.unvisited == false) {
                sb.append(n.name).append(":");
//                out.write(n.name.getBytes());
//                out.write(":".getBytes());
                for (int j = 0; j < n.subNodes.size(); j++) {
                    sb.append(" ").append(n.subNodes.get(j));
//                    out.write(" ".getBytes());
//                    out.write(n.subNodes.get(j).getBytes());
                }
                if (n.commands.toString() != null) {
                    sb.append(n.commands.toString());
//                    out.write(n.commands.toString().getBytes());
                }
            } else {
//                out.write("#".getBytes());
//                out.write(n.name.getBytes());
//                out.write(":".getBytes());
                sb.append("#").append(n.name).append(":");
                for (int j = 0; j < n.subNodes.size(); j++) {
                    sb.append(" ").append(n.subNodes.get(j));
//                    out.write(" ".getBytes());
//                    out.write(n.subNodes.get(j).getBytes());
                }
                if (n.commands.toString() != null) {
                    StringTokenizer tokenizer = new StringTokenizer(n.commands.toString(), "\n");
                    while (tokenizer.hasMoreTokens()) {
                        sb.append("\n").append("#").append(tokenizer.nextToken());
//                        out.write("\n".getBytes());
//                        out.write("#".getBytes());
//                        out.write(tokenizer.nextToken().getBytes());
                    }
                }
            }
            if (i < nodes.size() - 1) {
                sb.append("\n");
//                out.write("\n".getBytes());
            }
            System.out.print(sb);
            sb.delete(0, sb.length());
        }

//        out.flush();
    }

    private static Node getUnvisitedChildNode(Node node) {
        Node child;
        for (int i = 0; i < node.subNodes.size(); i++) {
            child = (Node) nodes.get(node.subNodes.get(i));
            if (!child.open && child.unvisited) {
                return child;
            } else if (child.open) {
                System.out.println("ERROR");
                System.exit(0);
            }
        }
        return null;
    }
}

class Node {

    String name;
    ArrayList<String> subNodes;
    StringBuilder commands = new StringBuilder();
    boolean unvisited = true;
    boolean open = false;
    boolean defined = false;
    Node next = null;

    Node(String n) {
        this.name = n;
        subNodes = new ArrayList<String>();
    }
}