package pal9;

import java.io.*;
import java.util.*;

public class Main9 {

    static ArrayList<Node> graph;
    static int numNodes;
    static int numEdges;

    public static void main(String[] args) throws IOException {
        initialize();
        Algoritmistan state = new Algoritmistan(tarjan(graph));
        state.printRegions();
    }

    public static final void initialize() throws IOException {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("inputC.txt"));
        } catch (FileNotFoundException ex) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        StringTokenizer st = new StringTokenizer(br.readLine());
        numNodes = toInt(st.nextToken());
        numEdges = toInt(st.nextToken());
        graph = new ArrayList<Node>(numNodes);
        for (int i = 0; i < numNodes; i++) {
            st = new StringTokenizer(br.readLine());
            if (st.countTokens() > 1) {
                graph.add(new Node(i, toInt(st.nextToken()), toInt(st.nextToken()), true));
            } else {
                graph.add(new Node(i, 0, toInt(st.nextToken()), false));
            }
        }
        for (int i = 0; i < numEdges; i++) {
            st = new StringTokenizer(br.readLine());
            Node from = graph.get(toInt(st.nextToken()) - 1);
            Node to = graph.get(toInt(st.nextToken()) - 1);
            from.adjacencies.add(to);
        }
    }

    public static final int toInt(String str) {
        int i = 0;
        int num = 0;
        while (i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0';
        }
        return num;
    }

    private static final void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + ", ");
            }
            System.out.println("");
        }
        System.out.println("");

    }

    private static final void printArray(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
    }

    public static final int[] toInts(String str) {
        int[] res = new int[str.length()];
        char[] ch = str.toCharArray();
        for (int j = 0; j < ch.length; j++) {
            res[j] = Character.getNumericValue(ch[j]);
        }
        return res;
    }

    public static List<Region> tarjan(List<Node> graph) {
        int n = graph.size();
        int[] stack = new int[n];
        int st = 0;
        int[] stack_cur = new int[n];
        int[] stack_pos = new int[n];
        int[] stack_prev = new int[n];
        int[] index = new int[n];
        Arrays.fill(index, -1);
        int[] lowlink = new int[n];
        int time = 0;
        List<Region> components = new ArrayList();

        for (int u = 0; u < n; u++) {
            if (index[u] == -1) {
                int top = 0;
                stack_cur[top] = u;
                stack_pos[top] = 0;
                stack_prev[top] = -1;
                while (top >= 0) {
                    int cur = stack_cur[top];
                    int pos = stack_pos[top];
                    if (index[cur] == -1) {
                        index[cur] = time;
                        lowlink[cur] = time;
                        ++time;
                        stack[st++] = cur;
                    }
                    if (pos < graph.get(cur).adjacencies.size()) {
                        int v = graph.get(cur).adjacencies.get(pos).num;
                        ++stack_pos[top];
                        if (index[v] == -1) {
                            ++top;
                            stack_cur[top] = v;
                            stack_pos[top] = 0;
                            stack_prev[top] = cur;
                        } else {
                            lowlink[cur] = Math.min(lowlink[cur], lowlink[v]);
                        }
                    } else {
                        int prev = stack_prev[top];
                        if (prev != -1) {
                            lowlink[prev] = Math.min(lowlink[prev], lowlink[cur]);
                        }
                        if (lowlink[cur] == index[cur]) {
                            List<Node> component = new ArrayList();
                            while (true) {
                                int v = stack[--st];
                                lowlink[v] = Integer.MAX_VALUE;
                                component.add(graph.get(v));
                                if (v == cur) {
                                    break;
                                }
                            }
                            Region r = new Region(component);
                            components.add(r);
                        }
                        --top;
                    }
                }
            }
        }
        return components;
    }
}

class Node {

    public final int num;
    public final int profit;
    public final int price;
    public boolean play;
    public final boolean isCity;
    public final ArrayList<Node> adjacencies;
    public boolean visited;

    public Node(int num, int price, int profit, boolean isCity) {
        this.num = num;
        this.profit = profit;
        this.price = price;
        this.isCity = isCity;
        this.play = false;
        this.visited = false;
        this.adjacencies = new ArrayList<Node>(Main.numNodes);
    }

    @Override
    public String toString() {
        if (isCity) {
            return (num + 1) + ". " + "M" + " " + price + "/" + profit;

        } else {
            return (num + 1) + ". " + "V" + " " + price;

        }
    }
}

class Algoritmistan {

    List<Region> regions;

    public Algoritmistan(List<Region> regions) {
        this.regions = regions;
        regions.get(regions.size() - 1).createStartTable();
        for (int i = 0; i < regions.get(regions.size() - 1).outputs.size(); i++) {
        }
    }

    public static int getBestPath() {

        return 0;
    }

    public void printRegions() {
        for (int i = 0; i < regions.size(); i++) {
            System.out.println(regions.get(i).nodes);
        }
    }
}

class Region {

    List<Node> nodes;
    List<int[][]> tables;
    List<Node> outputs;
    List<Node> inputs;
    int[][] actualTable;
    ArrayList<Path> paths;

    public Region(List<Node> nodes) {
        this.nodes = nodes;
        tables = new ArrayList<int[][]>(5);
    }

    public void findAllPaths(Node from, Node to) {
        Stack<Node> s = new Stack<Node>();
        dfs(from, to, s);
    }

    public void createStartTable() {
        actualTable = new int[nodes.size()][3]; // v tomto regionu bude jen jedna startovni
        tables.add(actualTable);
        for (int i = 0; i < nodes.size(); i++) { // vypln vsechny radky tabulky
            paths = new ArrayList<Path>(10);
            Node act = nodes.get(i);
            getPathsToActNode(act); // najdi vsechny cesty co vedou do tohoto uzlu

            actualTable[act.num][0] = -act.price;
            actualTable[act.num][1] = countCOL1(act);
            actualTable[act.num][2] = countCOL2(act);
        }
    }

    public void createNotStartTable(Node input) {
        actualTable = new int[nodes.size()][3];
        tables.add(actualTable);
        for (int i = 0; i < nodes.size(); i++) {
            Node act = nodes.get(i);
            actualTable[act.num][0] = countCOL0(act);
            actualTable[act.num][1] = countCOL1(act);
            actualTable[act.num][2] = countCOL2(act);
        }
    }

    private int countCOL0(Node act) {
        return 0;
    }

    private int countCOL1(Node act) {
        int bestProfit = Integer.MIN_VALUE;
        for (int i = 0; i < paths.size(); i++) {
            if (paths.get(i).numOfCities > 1) {
                // projed mesta a vzdycky poradej v jednom
            } else if (paths.get(i).numOfCities < 1) {
                return Integer.MIN_VALUE;
            } else {
                // je tam jedno mesto
            }
        }

        return bestProfit;
    }

    private int countCOL2(Node act) {
        int bestProfit = Integer.MIN_VALUE;
        for (int i = 0; i < paths.size(); i++) {
            if (paths.get(i).numOfCities > 2) {
                // generuj kombinace
                //        int[] array = new int[]{0, 1, 2, 3, 4};
//        int size = 2;
//        int[] tmp = new int[size];
//        Arrays.fill(tmp, -1);
//        ArrayList<int[]> combinations = new ArrayList<int[]>();
//        generateCombinations(array, 0, 0, tmp,combinations);
            } else if (paths.get(i).numOfCities < 2) {
                return Integer.MIN_VALUE;
            } else {
                // jsou tam dve mesta poradej v obou
            }
        }
        return bestProfit;
    }

    private void getPathsToActNode(Node act) {
        for (int i = 0; i < nodes.size(); i++) {
            if (!nodes.get(i).equals(act)) {
                findAllPaths(nodes.get(i), act );
            }
        }
    }

    private void generateCombinations(int[] array, int start, int depth, int[] tmp, ArrayList<int[]> output) {

        if (depth == tmp.length) {
            int[] out = new int[depth - 1];
            for (int j = 0; j < depth; j++) {
                out[j] = array[tmp[j]];
//                System.out.print(array[tmp[j]] + " ");
            }
            output.add(out);
//            System.out.println("");
            return;
        }
        for (int i = start; i < array.length; i++) {
            tmp[depth] = i;
            generateCombinations(array, i + 1, depth + 1, tmp, output);
        }
    }

    public void dfs(Node from, Node to, Stack<Node> s) // depth-first search
    {
        Path p = new Path();
        paths.add(p);
        from.visited = true;
        p.addNode(from);
        s.push(from);

        while (!s.isEmpty()) {
            Node n = s.peek();
            Node v = getAdjUnvisitedVertex(n);
            if (v == null) {
                p = new Path();
                paths.add(p);
                s.pop();
            } else {
                if (v.equals(to)) {
                    p.addNode(v);
                    p = new Path();
                    paths.add(p);
                    s.pop();
                } else {
                    p.addNode(v);
                    v.visited = true;
                    s.push(v);
                }

            }
        }


        for (int j = 0; j < nodes.size(); j++) {
            nodes.get(j).visited = false;
        }
    }

    public Node getAdjUnvisitedVertex(Node v) {
        for (int i = 0; i < v.adjacencies.size(); i++) {
            if (!v.adjacencies.get(i).visited) {
                return nodes.get(i).adjacencies.get(i);
            }
        }
        return null;
    }
}

class Path {

    ArrayList<Node> nodes;
//    ArrayList<Integer> indexesOfCities;
    int numOfCities;

    public Path(ArrayList<Node> nodes) {
        this.nodes = nodes;
//        indexesOfCities = new ArrayList<Integer>();
    }

    public Path() {
        nodes = new ArrayList<Node>();
//        indexesOfCities = new ArrayList<Integer>();
    }

    public void addNode(Node n) {
        nodes.add(n);
        if (n.isCity) {
            numOfCities++;
        }
    }
}
