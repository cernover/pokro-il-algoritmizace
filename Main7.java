package pal7;

import java.io.*;
import java.util.*;

public class Main7 {

    public static int numOfStates;
    public static int numOfChars;
    public static int Q;
    public static Node[] graph;
    public static char[] alphabeth = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    public static boolean isFinite = true;

    public static void main(String[] args) throws IOException {
        initialize();
        dfs();
        StringBuilder sb = new StringBuilder();
        if (isFinite) {
            sb.append("FINITE").append(" ");
            if (graph[0].maxWord.length() == 0) {
                sb.append("EPSILON");
            } else {
                sb.append(graph[0].maxWord);
            }

        } else {
            sb.append("INFINITE").append(" ");
            if (graph[0].minWord.length() == 0) {
                sb.append("EPSILON");
            } else {
                sb.append(graph[0].minWord);
            }
        }
        System.out.println(sb);
//        BufferedReader br = new BufferedReader(new FileReader("datapub/pub100.out"));
//        String output = br.readLine();
//        System.out.println(output);
    }

    public static final void initialize() throws IOException {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("datapub/pub100.in"));
        } catch (FileNotFoundException ex) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        StringTokenizer st = new StringTokenizer(br.readLine());
        numOfStates = toInt(st.nextToken());
        numOfChars = toInt(st.nextToken());
        graph = new Node[numOfStates];
        for (int i = 0; i < numOfStates; i++) {
            graph[i] = new Node(i);
        }
        Q = toInt(st.nextToken());
        if (Q == 1) {
            for (int i = 0; i < numOfStates; i++) {
                st = new StringTokenizer(br.readLine());
                st.nextToken();
                int numOfChar = 0;
                while (st.hasMoreTokens()) {
                    int numOfEdges = toInt(st.nextToken());
                    for (int j = 0; j < numOfEdges; j++) {
                        int indexTo = toInt(st.nextToken());
                        findEdge(i, indexTo, numOfChar);
                    }
                    numOfChar++;
                }
            }
        } else {
            st = new StringTokenizer(br.readLine());
            int B = toInt(st.nextToken());
            int C = toInt(st.nextToken());
            int T = toInt(st.nextToken());
            int U = toInt(st.nextToken());
            int V = toInt(st.nextToken());
            int W = toInt(st.nextToken());
            createGraph(B, C, T, U, V, W);
        }
        st = new StringTokenizer(br.readLine());
        int numOfFinalStates = toInt(st.nextToken());
        for (int i = 0; i < numOfFinalStates; i++) {
            graph[toInt(st.nextToken())].isFinal = true;
        }
    }

    public static final int toInt(String str) {
        int i = 0;
        int num = 0;
        while (i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0';
        }
        return num;
    }

    private static void findEdge(int indexFrom, int indexTo, int numOfChar) {
        if (graph[indexFrom].children.containsKey(indexTo)) {
            if (numOfChar < graph[indexFrom].children.get(indexTo).firstChar) {
                graph[indexFrom].children.get(indexTo).firstChar = numOfChar;
            }
            if (numOfChar > graph[indexFrom].children.get(indexTo).lastChar) {
                graph[indexFrom].children.get(indexTo).lastChar = numOfChar;
            }
        } else {
            Edge e = new Edge(graph[indexFrom], graph[indexTo], numOfChar);
            graph[indexFrom].children.put(indexTo, e);
        }
    }

    private static void createGraph(int B, int C, int T, int U, int V, int W) {
        int NB = (int) Math.ceil(numOfStates / B);
        for (int from = 0; from < numOfStates; from++) {
            for (int character = 0; character < numOfChars; character++) {
                int hs = T + ((U * from + V * character) % W);
                if (from >= NB) {
                    for (int k = 1; k <= hs; k++) {
                        int to = NB + ((B * from * k + C * character) % (numOfStates - NB));
                        findEdge(from, to, character);
                    }
                } else {
                    for (int k = 1; k <= hs; k++) {
                        int to = 1 + from + ((B * from * k + C * character) % (numOfStates - NB - 1));
                        findEdge(from, to, character);
                    }
                }
            }
        }
    }

    public static void dfs() {
        Stack<Node> stack = new Stack<Node>();
        stack.push(graph[0]);
        graph[0].isOpen = true;
        while (!stack.isEmpty()) {
            Node node = stack.peek();
            if (!node.containCycle) {
                Node child = node.getNextNode();
                if (child != null) {
                    if (child.isFinal && !isFinite) {
                        child.isOpen = true;
                        child.actualParent = node;
                        node.containFinal = true;
                        propagateMinWordToParent(child, node);
                        node.actualParent.containFinal = true;
                        propagateMinWordToParent(node, node.actualParent);
                        node.isOpen = false;
                        stack.pop();
                    } else { // child is not final or automaton is finite
                        if (!child.isOpen) {
                            child.isOpen = true;
                            child.actualParent = node;
                            if (child.isFinal) {
                                node.containFinal = true;
                            }
                            stack.push(child);
                        } else { // child was visited
                            if (child.isFinal || node.isFinal) {
                                node.containFinal = true;
                                isFinite = false;
                            }
                            child.containCycle = true;
                        }
                    }

                } else { // child is null
                    if (node.isFinal || node.containFinal) {
                        Node parent = node.actualParent;
                        if (isFinite) {
                            if (parent != null) {
                                parent.containFinal = true;
                                propagateMinWordToParent(node, parent);
                                propagateMaxWordToParent(node, parent);
                            }
                        } else {
                            //System.out.println("automat je infinite");
                            if (parent != null) {
                                //System.out.println("parent neni null");
                                parent.containFinal = true;
                                propagateMinWordToParent(node, parent);
                            }
                        }
                    }
                    if (node.containCycle && node.containFinal) {
                        isFinite = false;
                    }
                    node.isOpen = false;
                    stack.pop();
                }
            } else { // contain cycle
                if (node.containFinal || node.isFinal) {
                    isFinite = false;
                }
                if (node.isFinal) { // contain cycle, is final ---> is infinite
                    Node parent = node.actualParent;
                    if (parent != null) {
                        parent.containFinal = true;
                        propagateMinWordToParent(node, parent);
                    } else {
                        node.minWord = new StringBuilder();
                    }
                }
                if (node.containFinal) {
                    Node parent = node.actualParent;
                    if (parent != null) {
                        parent.containFinal = true;
                        propagateMinWordToParent(node, parent);
                    }
                }
                node.isOpen = false;
                stack.pop();
            }
        }
    }

    public static void propagateMinWordToParent(Node node, Node parent) {
        if (parent.minWord.length() == 0) {
            parent.addCharToMin(parent.children.get(node.number).firstChar);
            if (!node.isFinal) {
                parent.addCharToMin(node.minWord);
            }
            return;
        }
        node.addCharMinAtStart(parent.children.get(node.number).firstChar);
        if (node.minWord.length() < parent.minWord.length()) {
            parent.minWord = new StringBuilder();
            if (!parent.isFinal) {
                parent.addCharToMin(node.minWord);
            }
        }
        if (node.minWord.length() == parent.minWord.length()) {
            if (node.minWord.toString().compareTo(parent.minWord.toString()) < 0) {
                parent.minWord = new StringBuilder();
                if (!parent.isFinal) {
                    parent.addCharToMin(node.minWord);
                }
            }
        }
        node.minWord.deleteCharAt(0);
    }

    public static void propagateMaxWordToParent(Node node, Node parent) {
        if (parent.maxWord.length() == 0) {
            parent.addCharToMax(parent.children.get(node.number).lastChar);
            parent.addCharToMax(node.maxWord);
            return;
        }
        node.addCharMaxAtStart(parent.children.get(node.number).lastChar);
        if (node.maxWord.length() > parent.maxWord.length()) {
            parent.maxWord = new StringBuilder();
            parent.addCharToMax(node.maxWord);
        }
        if (node.maxWord.length() == parent.maxWord.length()) {
            if (node.maxWord.toString().compareTo(parent.maxWord.toString()) > 0) {
                parent.maxWord = new StringBuilder();
                parent.addCharToMax(node.maxWord);
            }
        }
        node.maxWord.deleteCharAt(0);
    }
}

class Node {

    public Node actualParent = null;
    public boolean isFinal = false;
    public boolean isOpen = false;
    public boolean containCycle = false;
    public boolean containFinal = false;
    public HashMap<Integer, Edge> children;
    public int number;
    public StringBuilder maxWord;
    public StringBuilder minWord;
    public Collection<Edge> edges = null;
    public Iterator<Edge> it;

    public Node(int num) {
        number = num;
        children = new HashMap<Integer, Edge>();
        maxWord = new StringBuilder();
        minWord = new StringBuilder();
    }

//    public String toString() {
//        return String.valueOf(number) + ", isFinal " + isFinal + ", containCycle " + containCycle + ", containFinal " + containFinal + ", minW " + minWord.toString() + ", maxW " + maxWord.toString();
//    }
    public String toString() {
        return String.valueOf(number);
    }

    public void addCharToMax(int numChar) {
        maxWord.append(Main.alphabeth[numChar]);
    }

    public void addCharToMin(int numChar) {
        minWord.append(Main.alphabeth[numChar]);
    }

    public void addCharMinAtStart(int numChar) {
        minWord.insert(0, Main.alphabeth[numChar]);
    }

    public void addCharMaxAtStart(int numChar) {
        maxWord.insert(0, Main.alphabeth[numChar]);
    }

    Node getNextNode() {
        if (edges == null) {
            edges = children.values();
            it = edges.iterator();
        }
        if (it.hasNext()) {
            return it.next().to;
        } else {
            return null;
        }
    }

    void addCharToMin(StringBuilder minW) {
        minWord.append(minW);
    }

    void addCharToMax(StringBuilder maxW) {
        maxWord.append(maxW);
    }
}

class Edge {

    public Node from;
    public Node to;
    public int firstChar;
    public int lastChar;

    public Edge(Node from, Node to, int character) {
        this.from = from;
        this.to = to;
        this.firstChar = character;
        this.lastChar = character;
    }

    public String toString() {
        return from + " " + to + " " + firstChar + " " + lastChar;
    }
}
