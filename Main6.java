package pal6;

import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Main6 {

    static int n;
    static int[] score;
    static int[][] matrix;
    static int[] startPerm;
    static int counter = 0;
    static CombinationGenerator x;
    static Graph firstGraph = null;

    public static void main(String[] args) throws IOException {
        initialize();
        generateGraphs(matrix, 0);
        NonIsoGraphs.addNonIsoGraph(firstGraph);
        findNonIsomorphicGraphs();
        System.out.println(NonIsoGraphs.count);
    }

    public static void initialize() throws IOException {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("input6.txt"));
        } catch (FileNotFoundException ex) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        StringTokenizer st = new StringTokenizer(br.readLine());
        n = toInt(st.nextToken());
        score = new int[n];
        int nonNull = 0;
        for (int i = 0; i < n; i++) {
            score[i] = toInt(st.nextToken());
            if (score[i] != 0) {
                nonNull++;
            }
        }
        n = nonNull;
        matrix = new int[n][n];
        int[] score2 = new int[n];
        int count = 0;
        for (int i = 0; i < n; i++) {
            score2[i] = score[i];
            if (i > 0) {
                if (score[i] == score[i - 1]) {
                    count++;
                } else {
                    DegreesGroups.addDegree(count);
                    //degGroups.add(count);
                    count = 1;
                }
            } else {
                count++;
            }
        }
        DegreesGroups.addDegree(count);
        //degGroups.add(count);
        score = score2;
        startPerm = new int[n];
        x = new CombinationGenerator(n, 2);
    }

    public static int toInt(String str) {
        int i = 0;
        int num = 0;
        while (i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0';
        }
        return num;
    }

    public static void generateGraphs(int[][] matrix, int index) {
        //System.out.println("index " + index);
        if (isCompleted(matrix, index)) {
            if (index < n - 1) {
                //System.out.println("hotovo, vnoruji");
                generateGraphs(matrix, index + 1);
            } else {
                //System.out.println("vyrabim graf");
                //printMatrix(matrix);
                if(firstGraph == null){
                    firstGraph = new Graph(matrix);
                }
                else{
                    Graphs.addGraph(new Graph(matrix));
                }
            }
        } else {
            int scoreActual = getScore(matrix, index);
            if (scoreActual <= n - 1 - index) {
                String input = createString(matrix, index);
                ArrayList<int[]> combinations = Combination.combination(input, score[index] - scoreActual);
                for (int i = 0; i < combinations.size(); i++) {
                    int[] comb = combinations.get(i);
                    for (int j = 0; j < comb.length; j++) {
                        //System.out.println("pridavam hranu: " + comb[j]);
                        matrix[index][comb[j]] = 1;
                        matrix[comb[j]][index] = 1;
                        //printMatrix(matrix);
                    }
                    if (index + 1 < n) {
                        //System.out.println("vnoruji");
                        generateGraphs(matrix, index + 1);
                    }
                    for (int j = 0; j < comb.length; j++) {
                        //System.out.println("mazu matrix[" + (index) + "][" + comb[j] + "]");
                        matrix[index][comb[j]] = 0;
                        //System.out.println("mazu matrix[" + comb[j] + "][" + (index) + "]");
                        matrix[comb[j]][index] = 0;
                        //printMatrix(matrix);
                    }
                }
                //System.out.println("return 1");
                return;
            } else {
                //System.out.println("return 2");
                return;
            }
        }
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(matrix[i][j] + ", ");
            }
            System.out.println("");
        }
        System.out.println("");

    }

    private static int getScore(int[][] matrix, int index) {
        int score = 0;
        for (int i = 0; i < n; i++) {
            score += matrix[index][i];
        }
        return score;
    }

    private static boolean isCompleted(int[][] matrix, int index) {
        if (getScore(matrix, index) == score[index]) {
            return true;
        } else {
            return false;
        }
    }

    private static String createString(int[][] matrix, int index) {
        StringBuilder sb = new StringBuilder();
        for (int i = index + 1; i < n; i++) {
            if (!isCompleted(matrix, i)) {
                sb.append(i);
            }
        }
        return sb.toString();
    }

    private static void findNonIsomorphicGraphs() {
        for (int i = 0; i < Graphs.count; i++) {
            if (isNonIsomorphicWithAll(Graphs.graphs[i])) {
                NonIsoGraphs.addNonIsoGraph(Graphs.graphs[i]);
                //nonIsoGraphs.add(graphs.get(i));
            }
        }
    }

    private static boolean isNonIsomorphicWithAll(Graph g) {
        for (int i = 0; i < NonIsoGraphs.count; i++) {
            if (isIso(NonIsoGraphs.graphs[i], g)) {
                return false;
            }
        }
        return true;
    }

    private static void printArray(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
    }

    private static boolean checkPermutations(Graph pattern, Graph g, int depth, int[] permSoFar, int pos) {
        if (depth == DegreesGroups.count) {
            if (checkIso(permSoFar, pattern, g)) {
                counter++;
                return true;
            }
            return false;
        }
        int permSize = DegreesGroups.degrees[depth];
        int[] pS = new int[permSize];
        for (int i = 0; i < pS.length; i++) {
            pS[i] = i + pos;
        }
        Permutation p = new Permutation(pS);
        int[] perm;
        perm = p.p;
        for (int i = 0; i < permSize; i++) {
            permSoFar[pos] = perm[i];
            pos++;
        }
        if (checkPermutations(pattern, g, depth + 1, permSoFar, pos)) {
            return true;
        }
        pos -= permSize;
        while (p.permuteNext()) {
            perm = p.p;
            for (int i = 0; i < permSize; i++) {
                permSoFar[pos] = perm[i];
                pos++;
            }
            if (checkPermutations(pattern, g, depth + 1, permSoFar, pos)) {
                return true;
            }
            pos -= permSize;
        }
        return false;
    }

    private static boolean checkIso(int[] perm, Graph pattern, Graph g) {
        int[] combination;
        x.reset();
        boolean canMap = true;
        while (x.hasMore()) {
            combination = x.getNext();
            boolean existInPattern = existEdge(pattern, combination[0], combination[1]);
            boolean existInG = existEdge(g, perm[combination[0]], perm[combination[1]]);
            if (existInPattern && existInG || !existInPattern && !existInG) {
            } else {
                canMap = false;
                break;
            }
        }
        if (canMap) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean isIso(Graph pattern, Graph g) {
        int[] permSoFar = new int[n];
        checkPermutations(pattern, g, 0, permSoFar, 0);
        if (counter > 0) {
            counter = 0;
            return true;
        } else {
            return false;
        }
    }

    private static boolean existEdge(Graph g, int node1, int node2) {
        if (g.matrix[node1][node2] == 1) {
            return true;
        } else {
            return false;
        }
    }
}

class NonIsoGraphs {

    static int count = 0;
    static Graph[] graphs = new Graph[100];

    public static void addNonIsoGraph(Graph g) {
        graphs[count++] = g;
    }
}

class DegreesGroups {

    static int count = 0;
    static int[] degrees = new int[Main.n];

    public static void addDegree(int d) {
        degrees[count++] = d;
    }
}
class Graphs{
    static int count = 0;
    static Graph[] graphs = new Graph[5000];

    public static void addGraph(Graph g) {
        graphs[count++] = g;
    }
}
class Graph {

    int[][] matrix;

    public Graph(int[][] e) {
        matrix = new int[e.length][e.length];
        for (int i = 0; i < e.length; i++) {
            for (int j = 0; j < e.length; j++) {
                matrix[i][j] = e[i][j];
                if (e[i][j] == 1) {
                }
            }
        }
    }
}

class Combination {

    static ArrayList<int[]> comb;

    public static ArrayList<int[]> combination(String s, int k) {
        
        comb = new ArrayList<int[]>();
        String head = "";
        String tail = s;
        combination(head, tail, k);
        return comb;
    }

    public static void combination(String head, String tail, int k) {
        if (tail.length() < k) {
            return;
        } else if (k == 0) {
            comb.add(toInts(head));
            // System.out.println(head);
        } else {
            String new_tail = tail.substring(1);
            String new_head = head + tail.charAt(0);
            combination(new_head, new_tail, k - 1);
            combination(head, new_tail, k);
        }
    }

    public static int[] toInts(String str) {
        int[] res = new int[str.length()];
        char[] ch = str.toCharArray();
        for (int j = 0; j < ch.length; j++) {
            res[j] = Character.getNumericValue(ch[j]);
        }
        return res;
    }
}

class Permutation {

    int[] p = new int[Main.n];
    int count;
    
    public Permutation(int[] word) {
        p = word;
    }

    public void setNewP(int[] p) {
        this.p = p;
        count = p.length;
    }

    public boolean permuteNext() {

        int i, j;
        i = p.length - 2;
        while (i >= 0 && p[i] > p[i + 1]) {
            i--;
        }
        if (i < 0) {
            return false;
        }
        j = p.length - 1;
        while (p[j] < p[i]) {
            j--;
        }
        int tmp = p[i];
        p[i] = p[j];
        p[j] = tmp;
        int x = i + 1, y = p.length - 1;
        while (x < y) {
            tmp = p[x];
            p[x++] = p[y];
            p[y--] = tmp;
        }
        return true;
    }

    public static int[] parseInts(String s, int size) {
        int[] output = new int[size];
        int j = 0;
        String num = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != 32) {
                num += s.charAt(i);
            } else {
                output[j] = Main.toInt(num);
                num = "";
                j++;
            }
        }
        output[j] = Main.toInt(num);
        return output;
    }
}

class CombinationGenerator {

    private int[] a;
    private int n;
    private int r;
    private BigInteger numLeft;
    private BigInteger total;

    public CombinationGenerator(int n, int r) {
        if (r > n) {
            throw new IllegalArgumentException();
        }
        if (n < 1) {
            throw new IllegalArgumentException();
        }
        this.n = n;
        this.r = r;
        a = new int[r];
        BigInteger nFact = getFactorial(n);
        BigInteger rFact = getFactorial(r);
        BigInteger nminusrFact = getFactorial(n - r);
        total = nFact.divide(rFact.multiply(nminusrFact));
        reset();
    }

    public void reset() {
        for (int i = 0; i < a.length; i++) {
            a[i] = i;
        }
        numLeft = new BigInteger(total.toString());
    }

    public BigInteger getNumLeft() {
        return numLeft;
    }

    public boolean hasMore() {
        return numLeft.compareTo(BigInteger.ZERO) == 1;
    }

    public BigInteger getTotal() {
        return total;
    }

    private static BigInteger getFactorial(int n) {
        BigInteger fact = BigInteger.ONE;
        for (int i = n; i > 1; i--) {
            fact = fact.multiply(new BigInteger(Integer.toString(i)));
        }
        return fact;
    }

    public int[] getNext() {
        if (numLeft.equals(total)) {
            numLeft = numLeft.subtract(BigInteger.ONE);
            return a;
        }
        int i = r - 1;
        while (a[i] == n - r + i) {
            i--;
        }
        a[i] = a[i] + 1;
        for (int j = i + 1; j < r; j++) {
            a[j] = a[i] + j - i;
        }
        numLeft = numLeft.subtract(BigInteger.ONE);
        return a;
    }
}
