# Pokročilá algoritmizace #

### Zadání ###
* [Úloha 1 - topologické uspořádání grafu](https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=makefile_refactoring)
* [Úloha 2 - „hedgehogMST“](https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=hedgehogMST)
* [Úloha 3 - „Maintenance“](https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=maintenance)
* [Úloha 4 - „Building Binomial Heaps“](https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=binomialheaps2)
* [Úloha 5 - „Counting spanning trees“](https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=counting_spanning_trees)
* [Úloha 6 - „Small Graphs Isomorphism“](https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=isomorphism)
* [Úloha 7 - „Language accepted by NFA“](https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=nfa_language)
* [Úloha 8 - „Dictionary automaton“](https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=dictionarynfa)
* [Úloha 9 - „Travelling circus“](https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=circus)