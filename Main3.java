package pal3;

import java.io.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.StringTokenizer;

public class Main3 {

    public static void main(String[] args) throws IOException {
        Euler e = new Euler(initialize());
        e.printPath();
    }

    public static Node[] initialize() throws IOException {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("input3.txt"));
        } catch (FileNotFoundException ex) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        int numOfNodes = toInt(br.readLine());
        Node[] nodes = new Node[numOfNodes];
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = new Node(i);
        }
        boolean hasNext = true;
        while (hasNext) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int from = toInt(st.nextToken());
            int to = toInt(st.nextToken());
            if (from == to && from == 0) {
                hasNext = false;
            } else {
                nodes[from].out.add(nodes[to]);
                nodes[to].inDegree++;
            }
        }
        return nodes;
    }

    public static int toInt(String str) {
        int i = 0;
        int num = 0;
        while (i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0';
        }
        return num;
    }
}

class Node {

    int num;
    int inDegree;
    Queue<Node> out;

    public Node(int n) {
        this.num = n;
        out = new LinkedList<Node>();
    }

    @Override
    public String toString() {
        return String.valueOf(num);
    }
}

class Euler {

    Stack<Node> toVisit;
    Stack<Node> path;
    Node[] nodes;

    public Euler(Node[] nodes) {
        this.nodes = nodes;
        toVisit = new Stack<Node>();
        path = new Stack<Node>();
    }

    private int getFirstNode() {
        int notSameDegree = 0;
        int index1 = -1;
        int index2 = -1;
        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i].inDegree != nodes[i].out.size()) {
                if (notSameDegree < 2) {
                    notSameDegree++;
                    if (nodes[i].inDegree + 1 > nodes[i].out.size() && index1 == -1) {
                        index1 = i;
                    } else if (nodes[i].inDegree < nodes[i].out.size() + 1 && index2 == -1) {
                        index2 = i;
                    } else {
                        return -1;
                    }
                } else {
                    return -1;
                }
            }
        }
        if (index1 != -1 && index2 == -1 || index2 != -1 && index1 == -1) {
            return -1;
        }
        if (index2 != -1) {
            return index2;
        } else {
            return 0;
        }
    }

    public void printPath() {
        int index = getFirstNode();
        if (index == -1) {
            System.out.println("-1");
            System.exit(0);
        }
        Node actual = nodes[index];
        while (!toVisit.empty() || !actual.out.isEmpty()) {
            if (actual.out.isEmpty()) {
                path.push(actual);
                actual = toVisit.pop();
            } else {
                toVisit.push(actual);
                actual = actual.out.remove();
            }
        }
        if (path.isEmpty()) {
            System.out.println("1 0 0");
            System.exit(0);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(nodes.length).append("\n");
            sb.append(nodes[index]).append(" ").append(path.get(path.size() - 1)).append("\n");
            for (int i = path.size() - 1; i > 0; i--) {
                sb.append(path.get(i)).append(" ").append(path.get(i - 1)).append("\n");
            }

            sb.append("0 0");
            System.out.println(sb);
        }
    }
}
