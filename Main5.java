package pal5;

import java.io.*;
import java.util.StringTokenizer;

public class Main5 {

    static double[][] matrix;

    public static void main(String[] args) throws IOException {
        initialize();
        //printMatrix();
        Matrix A = new Matrix(matrix);
        System.out.println(Math.round(Math.abs(A.det())));
    }

    public static void initialize() throws IOException {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("input5.txt"));
        } catch (FileNotFoundException ex) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        int n = toInt(br.readLine());
        if(n==1){
            System.out.println(1);
            System.exit(0);
        }
        matrix = new double[n -1][n-1 ];
        StringTokenizer st = new StringTokenizer(br.readLine());
        int a = toInt(st.nextToken());
        int b = toInt(st.nextToken());
        while (a != 0 || b != 0) {
            if (b > 0) {
                matrix[b-1][b-1]++;
            }
           if (a > 0) {
                matrix[a-1][a-1]++;
            }
            if (a > 0 && b > 0) {
                matrix[a-1][b-1] = -1;
                matrix[b-1][a-1] = -1;
            }
            st = new StringTokenizer(br.readLine());
            a = toInt(st.nextToken());
            b = toInt(st.nextToken());
        }
    }

    public static void printMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + ", ");
            }
            System.out.println("");
        }
    }

    public static int toInt(String str) {
        int i = 0;
        int num = 0;
        while (i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0';
        }
        return num;
    }
}
