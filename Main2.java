package pal2;

import java.io.*;
import java.math.BigInteger;
import java.util.*;

public class Main2 {

    public static void main(String[] args) throws IOException {
        Kruskal minTree = initialize();
        Collections.sort(minTree.allEdges);
        int[] indices;
        int min = Integer.MAX_VALUE;
        CombinationGenerator x = new CombinationGenerator(minTree.A.length, minTree.k);
        int[] kSet;
        while (x.hasMore()) {
            kSet = new int[minTree.k];
            indices = x.getNext();
            for (int i = 0; i < indices.length; i++) {
                kSet[i] = (minTree.A[indices[i]]);
            }
            int result = minTree.getRatingOfCombination(kSet, min);
            if (result < min) {
                if (result != -1) {
                    min = result;
                }
            }
        }
        if (min == Integer.MAX_VALUE) {
            System.out.println(-1);
        } else {
            System.out.println(min);
        }
    }

    private static Kruskal initialize() throws IOException {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("input2.txt"));
        } catch (FileNotFoundException ex) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        Kruskal minTree = new Kruskal();
        minTree.allEdges = new ArrayList<Edge>();
        int numOfNodes = toInt(br.readLine());
        minTree.allVertices = new Vertex[numOfNodes];
        for (int i = 0; i < numOfNodes; i++) {
            minTree.allVertices[i] = new Vertex(i);
        }
        minTree.numOfVertices = numOfNodes;
        for (int i = 0; i < numOfNodes; i++) {
            char[] chars = br.readLine().toCharArray();
            int weight = -1;
            int[] weights = new int[numOfNodes];
            int index = 0;
            for (int j = 0; j < chars.length; j++) {
                if (chars[j] != ' ') {
                    if (weight == -1) {
                        weight = 0;
                    }
                    weight = (chars[j] - '0') + 10 * weight;
                } else if (weight != -1) {
                    weights[index++] = weight;
                    weight = -1;
                }
            }
            if (weight != -1) {
                weights[index++] = weight;
            }
            for (int j = i; j < numOfNodes; j++) {
                int w = weights[j];
                if (w != 0) {
                    Edge e = new Edge(i, j, w);
                    e.vA = minTree.allVertices[i];
                    e.vB = minTree.allVertices[j];
                    minTree.allVertices[i].edges.add(e);
                    minTree.allVertices[j].edges.add(e);
                    minTree.allEdges.add(e);
                    minTree.addEdge(e);
                }
            }
        }
        StringTokenizer st = new StringTokenizer(br.readLine());
        int sizeOfA = toInt(st.nextToken());
        minTree.A = new int[sizeOfA];
        for (int j = 0; j < sizeOfA; j++) {
            minTree.A[j] = toInt(st.nextToken()) - 1;
        }
        minTree.k = toInt(br.readLine());
        return minTree;
    }

    public static int toInt(String str) {
        int i = 0;
        int num = 0;
        while (i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0';
        }
        return num;
    }
}

class Kruskal {

    int[] A;
    int k = 0;
    ArrayList<Edge> allEdges;
    HashMap<Integer, ArrayList<Edge>> edges = new HashMap<Integer, ArrayList<Edge>>();
    Vertex[] allVertices;
    int totalKruskalEdges = 0;
    int numOfVertices;

    public boolean isIn(int n, int[] kSet) {
        for (int i = 0; i < kSet.length; i++) {
            if (kSet[i] == n) {
                return true;
            }
        }
        return false;
    }

    public void addEdge(Edge e) {
        if (edges.containsKey(e.vertexA)) {
            edges.get(e.vertexA).add(e);
        } else {
            ArrayList<Edge> t = new ArrayList<Edge>();
            edges.put(e.vertexA, t);
            t.add(e);
        }
        if (edges.containsKey(e.vertexB)) {
            edges.get(e.vertexB).add(e);
        } else {
            ArrayList<Edge> t = new ArrayList<Edge>();
            edges.put(e.vertexB, t);
            t.add(e);
        }
    }

    public int countMST(int[] kSet, int min) {
        int total = 0;
        int n = 0;
        for (int i = 0; i < allEdges.size(); i++) {
            Edge e = allEdges.get(i);
            if (!e.removed) {
                Vertex parentA = findParent(e.vA);
                Vertex parentB = findParent(e.vB);
                if ((parentA == null || parentB == null) || !parentA.equals(parentB)) {
                    union(e.vA, e.vB, parentA, parentB);
                    total += e.weight;
                    n++;
                }
            }
            if (min < total) {
                return -1;
            } else if (n == (numOfVertices - k - 1)) {
                return total;
            }
        }
        if (n != (numOfVertices - k - 1)) {
            return -1;
        } else {
            return total;
        }
    }

    public int getRatingOfCombination(int[] kSet, int m) {
        for (int i = 0; i < kSet.length; i++) {
            ArrayList<Edge> edgs = edges.get(kSet[i]);
            for (Edge e : edgs) {
                e.removed = true;
            }

        }
        int total = countMST(kSet, m);
        if (total > 0) {
            for (int i = 0; i < kSet.length; i++) {
                Vertex v = allVertices[kSet[i]];
                int min = Integer.MAX_VALUE;
                for (int j = 0; j < v.edges.size(); j++) {
                    Edge e = v.edges.get(j);
                    if (!isIn(e.vertexA, kSet) || (!isIn(e.vertexB, kSet))) {
                        if (e.weight < min) {
                            min = e.weight;
                        }
                    }
                }
                if (min != Integer.MAX_VALUE) {
                    total += min;
                }
            }
        }
        for (int j = 0; j < allEdges.size(); j++) {
            allEdges.get(j).removed = false;
            allEdges.get(j).vA.parent = null;
            allEdges.get(j).vB.parent = null;
        }
        return total;
    }

    public Vertex findParent(Vertex v) {
        if (v.parent == null) {
            return null;
        } else {
            Vertex p = v.parent;
            while (p.parent != null && p.parent != p) {
                p = p.parent;
            }
            return p;
        }
    }

    public void union(Vertex vertexA, Vertex vertexB, Vertex parentA, Vertex parentB) {
        if (vertexA.parent == null && vertexB.parent == null) {
            vertexA.parent = vertexA;
            vertexB.parent = vertexA;
        } else {
            if (vertexA.parent == null) {
                vertexA.parent = findParent(vertexB);
            } else if (vertexB.parent == null) {
                vertexB.parent = findParent(vertexA);
            } else {
                parentA.parent = parentB;
            }
        }
    }
}

class Edge implements Comparable<Edge> {

    int vertexA, vertexB;
    Vertex vA, vB;
    int weight;
    boolean removed = false;

    public Edge(int vertexA, int vertexB, int weight) {
        this.vertexA = vertexA;
        this.vertexB = vertexB;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "(" + vertexA + "," + vertexB + "):Weight=" + weight + " " + removed + "\n";
    }

    public int compareTo(Edge edge) {
        return this.weight - edge.weight;
    }
}

class Vertex {

    int num;
    ArrayList<Edge> edges = new ArrayList<Edge>();
    Vertex parent;

    Vertex(int i) {
        this.num = i;
    }
}

class CombinationGenerator {

    private int[] a;
    private int n;
    private int r;
    private BigInteger numLeft;
    private BigInteger total;

    public CombinationGenerator(int n, int r) {
        if (r > n) {
            throw new IllegalArgumentException();
        }
        if (n < 1) {
            throw new IllegalArgumentException();
        }
        this.n = n;
        this.r = r;
        a = new int[r];
        BigInteger nFact = getFactorial(n);
        BigInteger rFact = getFactorial(r);
        BigInteger nminusrFact = getFactorial(n - r);
        total = nFact.divide(rFact.multiply(nminusrFact));
        reset();
    }

    public void reset() {
        for (int i = 0; i < a.length; i++) {
            a[i] = i;
        }
        numLeft = new BigInteger(total.toString());
    }

    public BigInteger getNumLeft() {
        return numLeft;
    }

    public boolean hasMore() {
        return numLeft.compareTo(BigInteger.ZERO) == 1;
    }

    public BigInteger getTotal() {
        return total;
    }

    private static BigInteger getFactorial(int n) {
        BigInteger fact = BigInteger.ONE;
        for (int i = n; i > 1; i--) {
            fact = fact.multiply(new BigInteger(Integer.toString(i)));
        }
        return fact;
    }

    public int[] getNext() {
        if (numLeft.equals(total)) {
            numLeft = numLeft.subtract(BigInteger.ONE);
            return a;
        }
        int i = r - 1;
        while (a[i] == n - r + i) {
            i--;
        }
        a[i] = a[i] + 1;
        for (int j = i + 1; j < r; j++) {
            a[j] = a[i] + j - i;
        }
        numLeft = numLeft.subtract(BigInteger.ONE);
        return a;
    }
}
