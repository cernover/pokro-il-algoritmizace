package pal8;

import java.io.*;
import java.util.*;

public class Main8 {

    public static int N, M;
    public static int[] T, P;
    public static int minLD;
    public static int[][] table;
    public static Graph g;

    public static void main(String[] args) throws IOException {
        initialize();
        table = computeLDtable(P, T);
        minLD = findMin(table[M]);
        g = new Graph(new Node(-1));
        if (N < 2000) {
            getDictRecursive();
        } else {
            getDict();
        }
        System.out.println(g.numOfNodes + 1);
    }

    public static final void initialize() throws IOException {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("input8.txt"));
        } catch (FileNotFoundException ex) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        StringTokenizer st = new StringTokenizer(br.readLine());
        N = toInt(st.nextToken());
        M = toInt(st.nextToken());
        int A1 = toInt(st.nextToken());
        int A2 = toInt(st.nextToken());
        int B = toInt(st.nextToken());
        int C = toInt(st.nextToken());
        int D = toInt(st.nextToken());
        int E = toInt(st.nextToken());
        long[] F = new long[N];
        T = new int[N];
        generateF(F, A1, B, C, D);
        generateTP(T, F, E);
        P = new int[M];
        generateF(F, A2, B, C, D);
        generateTP(P, F, E);
    }

    public static final int toInt(String str) {
        int i = 0;
        int num = 0;
        while (i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0';
        }
        return num;
    }

    private static void generateF(long[] F, int A, int B, int C, int D) {
        F[0] = new Long(A);
        int i = 0;
        while (++i < F.length) {
            F[i] = ((F[i - 1] + 1) * B + C) % D;
        }
//        for (int i = 1; i < F.length; i++) {
//         F[i] = ((F[i - 1] + 1) * B + C) % D;
//        }
    }

    private static void generateTP(int[] S, long[] FS, int E) {
        int i = -1;
        while (++i < S.length) {
            S[i] = (int) (FS[i] % E);
        }
//        for (int i = 0; i < S.length; i++) {
//            S[i] = (int) (FS[i] % E);
//        }
    }

    private static int findMin(int[] a) {
        int min = Integer.MAX_VALUE;
        int i = -1;
        while (++i < a.length) {
            if (a[i] < min) {
                min = a[i];
            }
        }
//        for (int i = 0; i < a.length; i++) {
//            if (a[i] < min) {
//                min = a[i];
//            }
//        }
        return min;
    }

    public static int[][] computeLDtable2(int from, int length) {
        int[][] ld = new int[P.length + 1][length + 1];
        int i = -1;
        while (++i < ld.length) {
            int j = -1;
            while (++j < ld[0].length) {
                ld[i][j] = i == 0 ? j : j == 0 ? i : 0;
                if (i > 0 && j > 0) {
                    if (P[i - 1] == T[from + j - 1]) {
                        ld[i][j] = ld[i - 1][j - 1];
                    } else {
                        ld[i][j] = Math.min(ld[i][j - 1] + 1, Math.min(ld[i - 1][j - 1] + 1, ld[i - 1][j] + 1));
                    }
                }
            }
        }
        return ld;
    }

    public static int[][] computeLDtable(int[] s1, int[] s2) {
        int[][] ld = new int[s1.length + 1][s2.length + 1];
        int i = -1;
        while (++i < ld.length) {
            ld[i][0] = i;
        }
        i = 0;
        while (++i < ld.length) {
            for (int j = 1; j < ld[i].length; j++) {
                ld[i][j] = i == 0 ? j : j == 0 ? i : 0;
                if (i > 0 && j > 0) {
                    if (s1[i - 1] == s2[j - 1]) {
                        ld[i][j] = ld[i - 1][j - 1];
                    } else {
                        ld[i][j] = Math.min(ld[i][j - 1] + 1, Math.min(ld[i - 1][j - 1] + 1, ld[i - 1][j] + 1));
                    }
                }
            }
        }
        return ld;
    }

    public static int computeLD(int from, int length) {
        int[][] ld = computeLDtable2(from, length);
        return ld[P.length][length];
    }

    public static void getDictRecursive() {
        int i = 0;
        while (++i <= N) {
            if (table[M][i] == minLD) {
                traceWord(M, M, i, i, 0);
            }
        }
    }

    public static int getMin(int A, int B, int C) {
        int smallest = A;
        if (smallest > B) {
            smallest = B;
        }
        if (smallest > C) {
            smallest = C;
        }
        return smallest;
    }

    public static void traceWord(int row, int actRow, int col, int actCol, int depth) {
        if (actCol == 0 || actRow == 0) {
            g.addWord(actCol, Math.abs(actCol - col));
            return;
        }
        if (T[actCol - 1] == P[actRow - 1]) {
            traceWord(row, actRow - 1, col, actCol - 1, depth + 1);
        } else {
            int min = Math.min(table[actRow][actCol - 1], Math.min(table[actRow - 1][actCol - 1], table[actRow - 1][actCol]));
            if (table[actRow][actCol - 1] == min) {
                traceWord(row, actRow, col, actCol - 1, depth + 1);
            }
            if (table[actRow - 1][actCol] == min) {
                traceWord(row, actRow - 1, col, actCol, depth + 1);
            }
            if (table[actRow - 1][actCol - 1] == min) {
                traceWord(row, actRow - 1, col, actCol - 1, depth + 1);
            }
        }
        return;
    }

    public static void getDict() {
        for (int i = 1; i <= N; i++) {
            if (table[M][i] == minLD) {
                int from = i - 1;
                for (int j = 1; j <= M + minLD; j++) {
                    if (from < 0) {
                        break;
                    }
                    if (computeLD(from, j) == minLD) {
                        g.addWord(from, j);
                    }
                    from--;
                }
            }
        }
    }
}

class Node {

    int num;
    Node next;
    boolean fin;
    HashMap<Integer, Node> children = new HashMap<Integer, Node>();

    public Node(int n) {
        num = n;
    }

    public String toString() {
        return num + " " + fin;
    }
}

class Graph {

    int numOfNodes;
    Node root;

    public Graph(Node root) {
        this.root = root;
    }

    public void addWord(int from, int length) {
        Node node = root;
        int i = from - 1;
        int to = from + length - 1;
        while (++i < to) {
            node = addChar(Main.T[i], false, node);
        }
        addChar(Main.T[from + length - 1], true, node);
    }

    public Node addChar(int n, boolean fin, Node start) {
        if (fin) {
            if (start.children.containsKey(n)) {
                if (start.children.get(n).fin) {
                    return start.children.get(n);
                } else {
                    Node node = new Node(n);
                    node.fin = true;
                    start.children.put(n, node);
                    numOfNodes++;
                    return node;
                }
            } else {
                Node node = new Node(n);
                node.fin = true;
                start.children.put(n, node);
                numOfNodes++;
                return node;
            }
        } else {
            if (start.children.containsKey(n)) {
                return start.children.get(n);
            } else {
                Node node = new Node(n);
                start.children.put(n, node);
                numOfNodes++;
                return node;
            }
        }
    }
}