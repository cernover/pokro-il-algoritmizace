package pal4;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * The class PermutationIterative computes all the Unique Permutations of a
 * string removing duplicates characters and whitespaces.
 *
 * @AUTHOR SHANTANU KHAN
 * http://0code.blogspot.cz/2012/09/Permutation-Iterative-Java.html
 */
public class Main4 {

    static int M, N;
    static int[] startPerm, endPerm;
    static Perm bestResult = new Perm(null, 0);
    static int[] oneLine;
    static int[] treeIndexes;
    static Permutation permutation;
    static int numOfTrees = 0;

    public static void main(String[] args) throws IOException {
        initialize();
        permutation = new Permutation(startPerm, M);
        countTreeIndexes();
        getNextExtPerm();
        countResult();
        while (permutation.permuteNext()) {
            if (!compareArrays()) {
                getNextExtPerm();
                countResult();
            } else {
                getNextExtPerm();
                countResult();
                break;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append(bestResult.result).append("\n");
        bestResult.printArray(sb, M);
        System.out.println(sb);
    }

    private static boolean compareArrays() {
        for (int i = 0; i < permutation.p.length - 1; i++) {
            if (permutation.p[i] != endPerm[i]) {
                return false;
            }
        }
        return true;
    }

    private static void countTreeIndexes() {
        char[] binaryChars = Integer.toBinaryString(N).toCharArray();
        treeIndexes = new int[binaryChars.length];
        for (int i = 0; i < binaryChars.length; i++) {
            if (binaryChars[i] == '1') {
                treeIndexes[numOfTrees] = (int) Math.pow(2, binaryChars.length - i - 1);
                numOfTrees++;
            }
        }
    }

    private static int getMaxMinusMin(int from, int to) {
        int max = -1;
        int min = Integer.MAX_VALUE;
        for (int i = from; i < to; i++) {
            if (oneLine[i] > max) {
                max = oneLine[i];
            }
            if (oneLine[i] < min) {
                min = oneLine[i];
            }
        }
        return max - min;
    }

    private static void getNextExtPerm() {
        int counter = M;
        if (N == M) {
            oneLine = new int[M * 2];
            for (int i = 0; i < M; i++) {
                oneLine[i] = permutation.p[i];
            }
            int i = 0;
            for (int j = permutation.p.length; j < oneLine.length; j++) {
                oneLine[j] = permutation.p[i++] + counter;
            }
        } else {
            oneLine = new int[N];
            for (int i = 0; i < M; i++) {
                oneLine[i] = permutation.p[i];
            }
            for (int i = M; i < oneLine.length; i++) {
                for (int j = 0; j < M; j++) {
                    oneLine[i + j] = permutation.p[j] + counter;
                }
                counter += M;
                i += M - 1;
            }
        }
    }

    public static void initialize() throws IOException {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("input4.txt"));
        } catch (FileNotFoundException ex) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        StringTokenizer st = new StringTokenizer(br.readLine());
        N = toInt(st.nextToken());
        M = toInt(st.nextToken());
        startPerm = Permutation.parseInts(br.readLine(), M);
        endPerm = Permutation.parseInts(br.readLine(), M);
    }

    public static int toInt(String str) {
        int i = 0;
        int num = 0;
        while (i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0';
        }
        return num;
    }

    private static void countResult() {
        int result = 0;
        int from = 0;
        int to = 0;
        for (int i = 0; i < numOfTrees; i++) {
            to += treeIndexes[i];
            result += getMaxMinusMin(from, to);
            from = to;
        }
        if (bestResult.result < result) {
            bestResult.p = oneLine;
            bestResult.result = result;
        }
    }
}

class Permutation {

    int[] p;
    int count;

    public Permutation(int[] word, int size) {
        p = word;
    }

    public boolean permuteNext() {
        int i, j;
        i = p.length - 2;
        while (i >= 0 && p[i] > p[i + 1]) {
            i--;
        }
        if (i < 0) {
            return false;
        }
        j = p.length - 1;
        while (p[j] < p[i]) {
            j--;
        }
        int tmp = p[i];
        p[i] = p[j];
        p[j] = tmp;
        int x = i + 1, y = p.length - 1;
        while (x < y) {
            tmp = p[x];
            p[x++] = p[y];
            p[y--] = tmp;
        }
        return true;
    }

    public void permute() {
    }

    public static int[] parseInts(String s, int size) {
        int[] output = new int[size];
        int j = 0;
        String num = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != 32) {
                num += s.charAt(i);
            } else {
                output[j] = Main.toInt(num);
                num = "";
                j++;
            }
        }
        output[j] = Main.toInt(num);
        return output;
    }
}

class Perm {

    int[] p;
    int result;

    public Perm(int[] s, int result) {
        this.p = s;
        this.result = result;
    }

    public void printArray(StringBuilder sb, int M) {
        for (int i = 0; i < M - 1; i++) {
            sb.append(p[i]).append(" ");
        }
        sb.append(p[M - 1]);
    }

    @Override
    public String toString() {
        return String.valueOf(result);
    }
}